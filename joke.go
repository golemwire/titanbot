package main

import (
	"math/rand"
	
	"github.com/bwmarrin/discordgo"
)

var (
	jokecmd_jokes = [...]string{

		// GENERAL:
		"The invention of the internal combustion engine *sparked* an ***explosive revolution*** that ***__pushed people forward :exploding_head:__***",
		"The fix to Apple's big FaceTime vulnerability should've been called iPatch.",
		"Online banking is dangerous; ||your computer might flush your cache!||",
		"In order to find the exact size of someone's legs, you need to decode their jeans.",
		"Ethernet is edible; ||I mean, computers take bytes from it!||",
		"As a new house owner, the recent flooding was a watershed event.",
		"No wonder many people hate math; ||The textbooks are full of so many problems!||",
		"I'm gonna go rob a car garage; ||I think I need to take a break.||",
		"I like building robots; ||It's riveting.||",
		"I can't speak the truth; ||I'm morally wounded.||",
		"I like fancy fridges; ||We're supposed to store food in a cool, dry place.||",
		"All candy store owners are Buddhists; ||When they set up shop, they enlighten-mint!||",
		"I'm always so tired and worn-out after school; ||It's the aftermath.||",
	//	"A hacker got in through the Windows. Better install ",
		"Tech companies value your privacy; ||That's why they steal it.||",
		"Error 101 Too Funny",
		"Even though my software is a CPU hog, it still comes out on `$ top`.",
		"It is said that magic and technology are indistinguishable. It's true! Neither of them work.",
		"You know those bridges that curve with the earth? What if they were straight and touched the water in the middle?\nNevermind, I don't want to go on a tangent.",
		"Teacher: when did World War II begin?\nStudent: wanna hear a history joke? Nah, I'm just Stalin'!",
		"I thought that I could \"miss\" some homework that my teacher assigned, but it's the principal that matters.",
		"Ever wonder why slow home improvement stores are the way they are? Maybe they are... ||**counter-productive!**||",
		
		// SATIRICAL:
		"A company stores intellectual property on a server that is made in China.\n||See https://www.cisa.gov/cybersecurity and https://www.bloomberg.com/news/features/2018-10-04/the-big-hack-how-china-used-a-tiny-chip-to-infiltrate-america-s-top-companies.||", // "A company stores intellectual property on a server that is made in China.\n||(*China*. Not speaking against the Chinese people per se.)||",
		"DRM",
		
		// LAMER:
		"How did the cake disappear? ||It ate itself!\n...that was my first joke... Lame(R)||",
		"What did the tabs-rather-than-spaces programmer say at a space-indented file? ||Tab-u-later!\n:face_vomiting: Lame(R)||",
		"It takes me a while to put my gear on to go outside in winter. I often will put my boots on *before* my snow pants; ||it looks like I need to adjust my boot priority.\nLame(R)||",
	//	"How do you escape a room with no windows or doors, when you only have your computer? ||Install Windows!||", // Mainly Jeb's joke
		"Many people commit bad code every day. It's a crime.\n||Get it? \"commit\"? Lame(R)||",
	}

	jokecmd = command {
		main: func(arg string, session *discordgo.Session, message *discordgo.MessageCreate) {
			session.ChannelMessageSend(message.ChannelID, jokecmd_jokes[rand.Intn(len(jokecmd_jokes))])
		},
		desc: "Hint: :rofl:",
	}
)
