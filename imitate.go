package main

import (
	"math/rand"
	"strings"
	"time"
	
	"github.com/bwmarrin/discordgo"
)

// The first map is a map of words to what words tend to follow (the next map). The next map is a map of words that tend to follow to occurences. If its key is empty, that means that the word ends the sentence.
type chain_t struct {
	c map[string]map[string]int
	// Words that start sentences. A word may appear in the array multiple times.
	sentStarters []string
}

func newChain(input string) (output chain_t) {
	output = chain_t{make(map[string]map[string]int), make([]string, 0, 16)}
	sentences := strings.FieldsFunc(input, func(r rune) bool {
		return r == '?' || r == '.' || r == '!'
	})
	
	for _, sen := range sentences {
		// TODO: OPTIMIZE: should delete all non-alphanumeric chars. ALSO, all of the words should have 1 trailing space, so that you can include punctuation as words! {
		sen = strings.ReplaceAll(sen, "\"", "")
		sen = strings.ToLower(sen)
		words := strings.Fields(sen)
		if len(words) != 0 {
			output.sentStarters = append(output.sentStarters, words[0])
			// }
			for i2, word := range words {
				var nextWord string
				if i2 + 1 == len(words) { // if at the last word
					nextWord = ""
				} else {
					nextWord = words[i2+1]
				}
				// TODO: inner map is nil SPOTIFY
				if _, ok := output.c[word]; !ok { // create the inner map if it does not exist:
					output.c[word] = make(map[string]int)
				}
				output.c[word][nextWord]++ // see the description of the chain type
			}
		}
	}
	return
}

// if seedWord is "", a random word from the first chain map will be selected.
// the len may exceed maxLen by a bit, maybe a word or a few chars.
// the chain should have at least one word in it.
func (chain *chain_t)gen(seedWord string, maxLen int) (output string) {
	chainLen := len(chain.c)
	//for _, _ := range chain { total++ }
	if chainLen == 0 { return }
	
	if seedWord == "" {
		// pick a random word from chain:
		rnd := rand.Intn(chainLen)
		i := 0
		for word, _ := range chain.c {
			if rnd == i {
				seedWord = word
				break
			}
			i++
		}
	} else {
		output = seedWord + " "
	}
	
	// Add to output, feeding the next word back into the loop as the seedWord:
	for len(output) <= maxLen { // note the break near the end of this loop
		// total the occurances:
		total := 0
		for _, occurs := range chain.c[seedWord] {
			total += occurs
		}
		if total == 0 { continue }
		// use the total and the chain ints to appropriately pick the word:
		// imagine: write each word in a box that is `occurs` inches wide. Line the boxes end-to-end. Then, put your finger at a random position along the line of boxes, and use the box you touch.
		// I *think* this code is accurately random:
		rnd := rand.Intn(total)
		for word, occurs := range chain.c[seedWord] {
			total -= occurs
			if total <= rnd {
				seedWord = word
				break
			}
		}
		if seedWord == "" { break }
		output += seedWord+" " // TODO: OPTIMIZE: I think this causes a lot of reallocations. Try an append()-style cap doubling technique.
	}
	
	// post-process the output for proper English syntax:
	if len(output) >= 5 {
		// TODO: perhaps choose a punctuation mark based on average punctuation marks!
		output = strings.ToUpper(output[0:1]) + output[1:len(output)-1] + "."
	}
	return
}

// Like chain.gen() (see it's comment), except a word from the start of a sentence from the original chain input is used to start a sentence.
func (chain *chain_t)gen_usingSentStarter(maxLen int) string {
	startWord := ""
	if len(chain.sentStarters) != 0 {
		startWord = chain.sentStarters[rand.Intn(len(chain.sentStarters))]
	}
	
	return chain.gen(startWord, maxLen)
}

// TODO: presently, you can make EthanBot read messages in channels that are from other servers!
// delay between sending the two messages!
// disable links or something!
// ignore (in the markov code) certain characters, like the double quote, etc.
// perhaps shuffle fetched messages before the concatination step.
// Hey! perhaps the first message is sent, then you say something, then it uses what you said to seed the next message :D!
func imitate(arg string, session *discordgo.Session, message *discordgo.MessageCreate) {
	// whether to read all user's messages, or just a mentioned one.
	var channelToSearch string
	if len(arg) < 21 || arg[1] != '#' { // if arg doesn't start by mentioning a channel
		channelToSearch = message.ChannelID
	} else {
		channelToSearch = arg[2:20]
	}
	session.ChannelTyping(message.ChannelID)
	
	// Get 100 channel messages:
	messages, err := session.ChannelMessages(channelToSearch, 100, "", "", "")
	handle(err)
	
	// Concatinate the user's messages together:
	userMessages := make([]string, 256)[:0]
	for i := len(messages)-1; i >= 0; i-- {
		m := messages[i]
		
		useMessage := false
		if len(message.Mentions) == 0 {
			// if command mentions no users, read this (therefore all) messages:
			useMessage = true
		} else {
			// use this message if its author is a mentioned user:
			for _, mentioned := range message.Mentions {
				if m.Author.ID == mentioned.ID { useMessage = true }
			}
		}
		if useMessage {
			// and split the message up into its words:
			split := strings.Fields(m.Content)
			for _, word := range split {
				userMessages = append(userMessages, word)
			}
		}
	}
	
	allowedMentions := discordgo.MessageAllowedMentions{/*Parse: []AllowedMentionType{}*/}
	rand.Seed(time.Now().UnixNano()) // Seed the random number generator.
	cmplxMessage := discordgo.MessageSend{AllowedMentions: &allowedMentions}
	c := newChain(strings.Join(userMessages, " ")) // FIXME:. also, use a strings.Builder for a lot!
	
	// generate and send two messages
	genCount := 0
	for i := 0; i < 5; i++ {
		if genCount == 0 {
			cmplxMessage.Content = c.gen_usingSentStarter(200)
		} else {
			cmplxMessage.Content = c.gen("", 200)
		}
		if cmplxMessage.Content != "" {
			session.ChannelMessageSendComplex(message.ChannelID, &cmplxMessage)
			genCount++
			// delay between 0 and 2 seconds:
			time.Sleep(time.Duration(rand.Intn(int(time.Second)*2)))
			if genCount == 2 {
				return
			}
		}
	}
	// if no messages were produced, imitate the user:
	if genCount == 0 {
		cmplxMessage.Content = "$ imitate "+arg
		session.ChannelMessageSendComplex(message.ChannelID, &cmplxMessage)
	}
}
