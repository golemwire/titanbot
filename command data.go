package main

import (
	"sync"
)

type cmdDatum struct {
	disabled bool
	runsSinceStart int
}

var (
	cmdData = &sync.Map{}
)

func init() {
	cmdData.Store("joke", cmdDatum{})
	
	cmdData.Store("help", cmdDatum{})
	cmdData.Store("ping", cmdDatum{})
	cmdData.Store("poll", cmdDatum{})
	cmdData.Store("imitate", cmdDatum{})
	cmdData.Store("rp", cmdDatum{})
	cmdData.Store("sentgen", cmdDatum{})
	cmdData.Store("cal", cmdDatum{})
	cmdData.Store("estop", cmdDatum{})
}
