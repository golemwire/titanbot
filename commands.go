package main

import (
	"strings"
	"os/exec"

	"github.com/bwmarrin/discordgo"
)

var commands map[string]*command

func init() {
	// Making the declaration and initialization separate this way prevents an initialization loop.
	commands = map[string]*command {
		"help": &command {
			main: func(arg string, session *discordgo.Session, message *discordgo.MessageCreate) {
				if arg != "" {
					command, ok := commands[arg]
					if ok {
						session.ChannelMessageSend(message.ChannelID, command.desc)
						return;
					}
				}
				session.ChannelMessageSend(
					message.ChannelID,
					`TitanBot is a bot for the Titanium Titans.

help: sends this message, *or* gives help on a specific command.
joke: outputs a joke
imitate: imitates messages

ping: sends "Pong!"

sentgen: generates a sentence
**For Titans only:**
cal: retrieves some events from the team Google calendar.
estop: emergency-stops TitanBot.`, // LABEL: INCLUDESNAME
				)
			},
			desc: `uh`,
		},
		"ping": &command {
			main: func(arg string, session *discordgo.Session, message *discordgo.MessageCreate) {
				session.ChannelMessageSend(message.ChannelID, "Pong!")
			},
			desc: `Sends "Pong!". Use it to test `+programName+`'s speed, and whether it's working.`,
		},
		"poll": &command { // TODO: finish this! See notebook
			main: func(arg string, session *discordgo.Session, message *discordgo.MessageCreate) {
				args := strings.Split(arg, " ")
				var newPoll Poll
				
				switch len(args) {
				case 0:
					newMessage, _ := session.ChannelMessageSend(message.ChannelID, message.Author.Mention()+"'s poll\nVote here!")
					session.MessageReactionAdd(newMessage.ChannelID, newMessage.ID, "👍")
					session.MessageReactionAdd(newMessage.ChannelID, newMessage.ID, "👎")
					newPoll = Poll{
						ownerID: message.Author.ID,
						messageID: message.ID,
						channelID: message.ChannelID,
					}
					addPoll(&newPoll)
					return
				case 1:
					if args[0] == "finish" {
						session.ChannelTyping(message.ChannelID)
						// finish! Use concludePollByUID
						return
					}
				}
				session.ChannelMessageSend(message.ChannelID, "Problem: try `$ help poll`")
			},
			desc: `[NOT DONE] Sends a message and reacts to it with a 👍 and 👎, so users may vote.
Use `+"`$ poll finish`"+` to end the poll.

If `+programName+` has the permissions to, it will remove the users' reactions.`,
		},
		"imitate": &command {
			main: imitate,
			desc: `Generates sentences that sound like what is said in a channel by everyone, a user, or multiple users that you specify.
`+"`"+`$ imitate #channel @user`+"`"+`
This command imitates by generating a *markov chain*. A markov chain (of this kind) is a model of which words are most likely to follow other words.`,
		},
		"sentgen": &command {
			main: func(arg string, session *discordgo.Session, message *discordgo.MessageCreate) {
				out, err := exec.Command("/opt/titanbot/sentgen").Output()
				handle(err)
				session.ChannelMessageSend(message.ChannelID, string(out))
			},
			desc: "Generates a randomized sentence.",
		},
		"cal": &command {
			main: func(arg string, session *discordgo.Session, message *discordgo.MessageCreate) {
				if isTitanMsg(message) || isAdminMsg(message) {
					calCmd <- message.ChannelID
				}
			},
			desc: "Fetches today's and tomorrow's events from the team Google calendar.",
		},
		"estop": &command {
			main: func(arg string, session *discordgo.Session, message *discordgo.MessageCreate) {
				if isTitanMsg(message) {
					session.ChannelMessageSend(message.ChannelID, ":exclamation: Emergency-stopping.")
					shutdn <- false
				}
			},
			desc: "Emergency-stops " + programName,
		},
		"joke": &jokecmd,
		// TODO: "quote": &quotecmd,
		// TODO: have a $ proverb command!
	}
}
