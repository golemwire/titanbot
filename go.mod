module ethan/titanbot

go 1.16

require (
	cloud.google.com/go v0.97.0 // indirect
	github.com/bwmarrin/discordgo v0.23.2
	github.com/golang/groupcache v0.0.0-20210331224755-41bb18bfe9da // indirect
	github.com/gorilla/websocket v1.4.2 // indirect
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519 // indirect
	golang.org/x/net v0.0.0-20211011170408-caeb26a5c8c0
	golang.org/x/oauth2 v0.0.0-20211005180243-6b3c2da341f1
	golang.org/x/sys v0.0.0-20211007075335-d3039528d8ac // indirect
	golang.org/x/text v0.3.7 // indirect
	google.golang.org/api v0.58.0
	google.golang.org/genproto v0.0.0-20211012143446-e1d23e1da178 // indirect
	google.golang.org/grpc v1.41.0 // indirect
)
