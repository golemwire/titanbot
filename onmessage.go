package main
// session.ChannelMessageSend(message.ChannelID, "")

import (
	"log"
	"strings"
	"strconv"
	"math/rand"
	"fmt"
	"time"
	
	"github.com/bwmarrin/discordgo"
)

const (
	titansCountingChnl = "400104371461029889"
	coopGamingCountingChnl = "901586474825302086"
)

var (
	easterEggsEnabled = true // TODO: root $ eggs command
)

func onMessage(session *discordgo.Session, message *discordgo.MessageCreate) {
	// Ignore all messages created by the bot itself
	if message.Author.ID == session.State.User.ID {
		return
	}
	
	if easterEggsEnabled && len(message.Mentions) == 1 && message.Mentions[0].ID == session.State.User.ID && rand.Intn(3) == 0 {
		session.ChannelMessageSend(message.ChannelID, randMentionedText())
	}
	
	defer func() {
		if p := recover(); p != nil {
			errorMessage := reportErr("Internal error in onMessage()", p)
			defer func() {
				if err := recover(); err != nil {
					log.Printf("Error in onMessage recovery: %v", p)
					shutdn<-true
				}
			}()
			session.ChannelMessageSend(message.ChannelID, errorMessage)
		}
	}()
	
	switch {
	case strings.HasPrefix(message.Content, "$ "):
		spaceIndex := strings.IndexByte(message.Content[2:], ' ')
		
		var commandString, arg string
		if spaceIndex == -1 {
			commandString = message.Content[2:]
			/* // user has just said "$ " >:( Discord trims spaces off of messages, so user shouldn't be able to just say "$ ".
			session.ChannelMessageSend(message.ChannelID, "You jerk! How did you do that!? :stuck_out_tongue_closed_eyes:")
			return*/
		} else {
			spaceIndex += 2
			commandString = message.Content[2:spaceIndex]
			arg = message.Content[spaceIndex+1:]
		}
		
		command, ok := commands[commandString]
		if ok {
			load, _ := cmdData.Load(commandString)
			datum := load.(cmdDatum)
			if datum.disabled && !(isAdminMsg(message)) { // if the command is disabled, excepting admin(s)
				session.ChannelMessageSend(message.ChannelID, "Problem: "+commandString+" is disabled!")
			} else {
				go fmt.Println(message.Content)

				//load, _ = cmdData.Load(commandString)
				datum.runsSinceStart++
				cmdData.Store(commandString, datum)
				command.main(arg, session, message)
			}
			return
		} else if isAdminMsg(message) {
			command, ok := rootCommands[commandString]
			if ok {
				log.Printf("ROOT: %v %v\n", commandString, arg)
				command.main(arg, session, message)
				return
			}
		}
		session.ChannelMessageSend(message.ChannelID, "Problem: "+commandString+" is not a command!")
	default: // extra, and easter eggs:
		// counting channel:
		if (message.ChannelID == titansCountingChnl || message.ChannelID == coopGamingCountingChnl) &&
		   (rand.Intn(6) == 0 ||
		   (time.Now().Hour()%2==0 && rand.Intn(2)==0) ) { // Respond often (1/2 chance) when the hour is divisible by 2, 1/6 chance otherwise
			content := strings.TrimRightFunc(message.Content, func(r rune) bool { // TODO: fix
				return r!='0'&&r!='1'&&r!='2'&&r!='3'&&r!='4'&&r!='5'&&r!='6'&&r!='7'&&r!='8'&&r!='9'
			})
			num, err := strconv.Atoi(content)
			if err != nil { return }
			
			num++
			if num == 4467 { return }
			
			session.ChannelMessageSend(message.ChannelID, strconv.Itoa(num))
		}
	
		/*if easterEggsEnabled && rand.Intn(50) == 0 {
			lc := strings.ToLower(message.Content)
			switch {
			case strings.Contains(lc , "[SOMETHING]")
			}
		}*/
	}
}

func randMentionedText() string {
	text := [...]string{"Huh? ","Really? ","Aha. ","Do you really think so :pleading_face: "}
	return text[rand.Intn(len(text))] + text[rand.Intn(len(text))]
}
