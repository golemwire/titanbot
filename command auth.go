package main

import (
	"github.com/bwmarrin/discordgo"
)

const (
	titanGuildID = "OMITTED"
)

// returns whether a message is in the TT guild.
func isTitanMsg(message *discordgo.MessageCreate) bool {
	return message.GuildID == titanGuildID
}

func isAdminMsg(message *discordgo.MessageCreate) bool { // TODO: verify this works!
	var adminIDs = map[string]bool{
		// Me, Golemwire
		"459527863318609930": true,
	}
	return adminIDs[message.Author.ID]
}
