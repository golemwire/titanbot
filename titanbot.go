package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"
	"math/rand"

	"github.com/bwmarrin/discordgo"
	"google.golang.org/api/calendar/v3"
)

type messageSend_t struct {channel string; message string}

const (
	programName = "TitanBot" // any other instances of the name are commented "INCLUDESNAME" preceded by "LABEL: "
	consoleChannel = "OMITTED" // #titanbot
)

var (
	bot *discordgo.Session
	gcal *calendar.Service
	
	messageSend = make(chan messageSend_t, 3)
	channelTyping = make(chan string)
	
	// true means restart, false means shutdown
	shutdn = make(chan bool, 1)
)

func main() {
	if run() {
		os.Exit(1)
	} else {
		os.Exit(0)
	}
}

// returns whether to restart TitanBot
func run() bool {
	rand.Seed(time.Now().UnixNano())
	
	bot, err := discordgo.New("Bot " + discordToken)
	handle(err)
	defer bot.Close()
	
	bot.AddHandler(onMessage)
	//bot.AddHandler(onReact)
	//bot.AddHandler(onReactRemove)
	//bot.AddHandler(onReactRemoveAll)
	
	// Open a websocket connection to Discord and begin listening.
	err = bot.Open()
	handle(err)
	
	log.Println(programName+" started.")
	
	gcal = initGcal()
	go gcalMonitor()
	
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	loop:
	select {
	case <-sc:
	case restart := <-shutdn:
		if restart {
			log.Println("Restarting...")
			return true
		}
	case message := <-messageSend:
		bot.ChannelMessageSend(message.channel, message.message)
		goto loop
	case channel := <-channelTyping:
		bot.ChannelTyping(channel)
		goto loop
	}
	log.Println("Stopping...")
	return false
}

func handle(err error) {
	if err != nil {
		panic(err)
	}
}

func reportErr(desc string, p interface{}) string {
	errorMessage := fmt.Sprintf(":warning: %v\nThe error has been reported.", desc)
	internalErrMessage := fmt.Sprintf("%v\n%v\n%v", time.Now(), desc, p)
	
	log.Println(internalErrMessage)
	fmt.Fprint(os.Stderr, "\a\a\a") // ␇ × 3
	messageSend<-messageSend_t{consoleChannel, internalErrMessage}
	
	return errorMessage
}
