#!/usr/bin/sh

HOST='user@server'
TARGET="$HOST:/opt/titanbot"

# You can add a file to /etc/sudoers.d to allow this for the a given user:
#     givenuser ALL=NOPASSWD: /usr/bin/systemctl stop titanbot.service
#     givenuser ALL=NOPASSWD: /usr/bin/systemctl start titanbot.service
#     givenuser ALL=NOPASSWD: /usr/bin/systemctl restart titanbot.service
echo 'sudo systemctl stop titanbot.service' | ssh "$HOST"

scp titanbot "$TARGET"
#scp run.sh "$TARGET"
#scp -r home "$TARGET"
#scp titanbot.service "$TARGET"

echo 'sudo systemctl start titanbot.service' | ssh "$HOST"
