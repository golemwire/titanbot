package main

// LATER: how do leap seconds affect this code?
// TODO/LATER: re-write the bulk of this. I don't like the control flow.

import (
	"fmt"
	"time"
	"log"
	
	"google.golang.org/api/calendar/v3"
)

const (
	announceChannel = "742125327232401418" // #events
//	announceChannel = consoleChannel
//	announceChannel = "400104315227865098" // #spot-bam
	
	secondsInDay = 24*60*60
	timeOffset = 4*60*60 // (in seconds) // TODO: debug! // Might just be a time zone difference, maybe use time.Now().UTC(). Note the -04:00 that appears in the gcal time, and the -0400 that appears in stat. Perhaps recode todayFlooredToDay to be more idiomatic; maybe try time.Date.
)

var (
	outputChannel string
	pingEveryone bool
//	// whether the announcement currently being made is due to $ cal.
//	doingCalCmd bool
	// send a channel ID through calCmd
	calCmd = make(chan string)
	calnow = make(chan int)
)

const (
	calnow_normal int = iota
	calnow_quiet
)

func gcalMonitor() {
	defer func() {
		if p := recover(); p != nil {
			fmt.Print("Error in gcalMonitor(): ") // TODO
			fmt.Println(p)
			shutdn<-true
		}
	}()
	
	var (
		events, laterEvents *calendar.Events
		timer *time.Timer
		waitTime time.Time
	)
	
	const (
		// The name, time, and location of the meeting
		announceFmtStr = "**%s** (**%s**)\n**Location:** %s"
		// "https://" is in the string because the Google Calendar API returns the Google Meet link without it.
		gmeetLinkFmtStr = "**Meeting link:** https://%s"
		pleaseRespondStr = "**Please respond whether you will attend, at https://calendar.google.com/**"
	)
	
	announce := func(outputChannel string, pingEveryone bool) {
		channelTyping <- outputChannel
		// announce today's events:
		if len(events.Items) == 0 {
			fmt.Println("No upcoming events found.")
		} else {
			var eventsString string
			if pingEveryone {
				eventsString = "@everyone **Today's events:**\n"
			} else {
				eventsString = "**Today's events:**\n"
			}
			for _, item := range events.Items {
				// Get and format the date nicely:
				date := item.Start.DateTime
				if date == "" {
					date = "(**all day event**) " + item.Start.Date // TODO: NEXT: format time reasonably
				} else {
					parsedDate, _ := time.Parse(time.RFC3339, date)
					date = parsedDate.Format("3:04 PM")
				}
				
				if item.ConferenceData != nil && len(item.ConferenceData.EntryPoints) != 0 && item.ConferenceData.EntryPoints[0].Label != "" { // If there's a Google Meet event:
					eventsString += fmt.Sprintf(announceFmtStr+"\n%s\n"+gmeetLinkFmtStr+"\n"+pleaseRespondStr+"\n\n**Next event:**\n",
						item.Summary, date, item.Location, replaceHTMLAndQuote(item.Description), item.ConferenceData.EntryPoints[0].Label)
				} else { // If there's no Google Meet event:
					eventsString += fmt.Sprintf(announceFmtStr+"\n%s\n"+pleaseRespondStr+"\n\n**Next event:**\n",
						item.Summary, date, item.Location, replaceHTMLAndQuote(item.Description))
				}
			}
			eventsString = eventsString[:len(eventsString)-18] // Chops off the "\n\n**Next event:**\n"
			
			messageSend <- messageSend_t{outputChannel, eventsString}
			if pingEveryone { log.Println(eventsString) }
		}
		
		// warn about some later events:
		if len(laterEvents.Items) == 0 {
			
		} else {
			eventsString := "**Tomorrow:**\n"
			for _, item := range laterEvents.Items {
				date := item.Start.DateTime
				if date == "" {
					date = "(**all day event**) " + item.Start.Date // TODO: NEXT: format time reasonably
				} else {
					parsedDate, _ := time.Parse(time.RFC3339, date)
					date = parsedDate.Format("3:04 PM Monday, January 2")
				}
				
				eventsString += fmt.Sprintf(announceFmtStr+"\n"+pleaseRespondStr+"\n\n**Next event of tomorrow:**\n", item.Summary, date, item.Location)
			}
			eventsString = eventsString[:len(eventsString)-(18+12)]
			
			messageSend <- messageSend_t{outputChannel, eventsString}
			if pingEveryone { log.Println(eventsString) }
		}
		if pingEveryone { log.Println("Announced.") }
	}

	wait := func() {
		for {
			select {
			case <-timer.C:
			case channel := <-calCmd: // when commanded to report events (like by $ cal)
				announce(channel, false)
				continue
			case calnowType := <-calnow: // TODO NEXT: I used this before noon, and it announced tomorrow's events (good) but then announced again at noon.
			//	timer.Stop() // release timer resources (?)
				announce(announceChannel, calnowType == calnow_normal)
				continue
			}
			break
		}
	}

	for {
		var err error
		events, laterEvents, err = checkGcal()
		handle(err)
		
		today := todayFlooredToDay()
		
		switch { // the "main switch". Note that this always sets the timer, or does goto tomorrow.
		case len(events.Items) != 0: // if there's an event,
			// wait until the first event's time minus 4h,
			// or if the first event is an all-day event, at 8:00AM:
			
			date := events.Items[0].Start.DateTime
			
			if date == "" { // if an all-day event:
				waitTime = today.Add(8*time.Hour) // announce at 8:00
			} else {
				waitTime, _ = time.Parse(time.RFC3339, date)
				waitTime = waitTime.Add(time.Hour * -4)
			}
			
			if until := time.Until(waitTime); until > 0 { // if waitTime is in the future:
				timer = time.NewTimer(until)
			} else {
				messageSend <- messageSend_t{consoleChannel, "NOTE: Missed event announcement at "+events.Items[0].HtmlLink}
				goto tomorrow
			}
		default: // wait until noon
			waitTime = today.Add(12 * time.Hour)
			if until := time.Until(waitTime); until > 0 {
				timer = time.NewTimer(until)
			} else {
				messageSend <- messageSend_t{consoleChannel, "NOTE: Missed (potential) noon event announcement"}
				goto tomorrow
			}
		}
		
		log.Printf("[gcalMonitor] waiting until %v\n", waitTime)
		wait()
		announce(announceChannel, true)
		
		tomorrow:
		// wait until tomorrow:
	//	time.Sleep(time.Until(today.Add(24*time.Hour)))
		timer = time.NewTimer(time.Until(today.Add(24*time.Hour)))
		wait()
	}
}

func replaceHTMLAndQuote(input string) (output string) {
	output = "> "
	// marks a char as the start of non-HTML-tag text
	previous := 0
	// whether current char is inside an HTML tag
	inTag := false
	for i := 0; i < len(input); i++ {
		switch input[i] {
		case '<': // HTML tag starts
			inTag = true
			// Add current text
			output += input[previous:i]
		case '>':
			inTag = false
			previous = i + 1 // The next character starts an area that isn't in an HTML tag
			
			switch {
			// If the tag was <br>, then add '\n> ':
			case (i >= 3 && input[i-3:i+1] == "<br>") || (i >= 4 && input[i-4:i+1] == "<br/>") || (i >= 5 && input[i-5:i+1] == "<br />"):
				output += "\n> "
			// List item:
			case (i >= 3 && input[i-3:i+1] == "<li>"):
				output += "\n>  •   "
			// Bold:
			case (i >= 2 && input[i-2:i+1] == "<b>") || (i >= 3 && input[i-2:i+1] == "</b>"):
				output += "**"
			// Italicized:
			case (i >= 2 && input[i-2:i+1] == "<i>") || (i >= 3 && input[i-2:i+1] == "</i>"):
				output += "*"
			// Underlined:
			case (i >= 2 && input[i-2:i+1] == "<u>") || (i >= 3 && input[i-2:i+1] == "</u>"):
				output += "__"
			}
		case '&':
			if !inTag {
				// TODO: add more HTML entities: &amp; (&) and more for '<' and '>' etc.
				if i + 5 < len(input) && input[i+1:i+6] == "nbsp;" {
					// Add current text:
					output += input[previous:i]
					// Skip it:
					i += 6
					previous = i + 1
					// Append what “&nbsp;” means; a space:
					output += " "
				}
			}
		/*case '\n':
			if !inTag {
				// Replace '\n' with "\n> " to quote text
				output += "\n> "
				// Add current text:
				output += input[previous:i]
				previous = i + 1
			}*/
		}
		// ⚠ i may be greater than the limit, in which case the loop should terminate.
	}
	if !inTag {
		output += input[previous:]
	}
	return
}

func checkGcal() (events, laterEvents *calendar.Events, err error) {
	today := todayFlooredToDay()
	tomorrow := today.AddDate(0, 0, 1)
	fmt.Print("Checking gcal events from "+today.String()+"..")
	
	todayRFC := today.Format(time.RFC3339)
	tomorrowRFC := tomorrow.Format(time.RFC3339) // OPTIMIZE: Is this really the first second of tomorrow, or the last second of today?
	laterStartRFC := tomorrowRFC //
	laterEndRFC := today.AddDate(0, 0, 2).Format(time.RFC3339) // start and end times of the 'later events announcement' events. NOTE: a string variable named eventsString is hardcoded to refer to these times.
	events, err = gcal.Events.List("primary").
	              ShowDeleted(false).
	              SingleEvents(true).
	              TimeMin(todayRFC).
	              TimeMax(tomorrowRFC).
	              OrderBy("startTime").
	              Do()
	if err != nil {
		reportErr("Unable to retrieve next gcal events", err)
		return
	}
	laterEvents, err = gcal.Events.List("primary").
	                   ShowDeleted(false).
	                   SingleEvents(true).
	                   TimeMin(laterStartRFC).
	                   TimeMax(laterEndRFC).
	                   OrderBy("startTime").
	                   Do()
	if err != nil {
		reportErr("Unable to retrieve next (later) gcal events", err)
		return
	}
	fmt.Printf("..%v events today, %v events tomorrow.\n", len(events.Items), len(laterEvents.Items))
	return
}

// time.Now() rounded down to the nearest day.
func todayFlooredToDay() time.Time {
	return time.Unix((time.Now().Unix()/secondsInDay)*secondsInDay + timeOffset, 0)
}
