package main

import (
	"fmt"
	"os"
	"os/exec"

	"github.com/bwmarrin/discordgo"
)

// This map contains "root" (so-to-speak) commands that only I can use.
var rootCommands = map[string]*command {
	"tog": &command { // TODO: should save toggled stuff to a file
		main: func(arg string, session *discordgo.Session, message *discordgo.MessageCreate) {
			
			if _, ok := commands[arg]; ok { // if the command exists:
				load, _ := cmdData.Load(arg)
				datum := load.(cmdDatum)
				datum.disabled = !datum.disabled
				cmdData.Store(arg, datum)
				// print what it is set to:
				if datum.disabled {
					session.ChannelMessageSend(message.ChannelID, "Command disabled!")
				} else {
					session.ChannelMessageSend(message.ChannelID, "Command enabled!")
				}
			} else {
				session.ChannelMessageSend(message.ChannelID, arg+" is not a command!")
			}
		},
	},
	"fail": &command{
		main: func(arg string, session *discordgo.Session, message *discordgo.MessageCreate) {
			session.ChannelMessageSend(message.ChannelID, reportErr("[No problem]", nil))
		},
	},
	"sys": &command {
		main: func(arg string, session *discordgo.Session, message *discordgo.MessageCreate) {
			switch arg {
			case "stop": // Shut down EthanBot
				session.ChannelMessageSend(message.ChannelID, "Stopping...")
				shutdn<-false
			case "": // Show status
				// TODO: do!
				// also check and print abnormalties, for debugging
				rcmdsStr := "Commands (name, runs-since-start):\n"
				cmdData.Range(func(key, value interface{}) bool {
					//rcmdsStr = append(rcmdsStr, key.(string) + " " + value.(cmdDatum).runsSinceStart)
					disabled := value.(cmdDatum).disabled
					if disabled {
						rcmdsStr += "[DISABLED] "
					}
					rcmdsStr += fmt.Sprintf("%s %v\n", key.(string), value.(cmdDatum).runsSinceStart)
					return true
				})
				session.ChannelMessageSend(message.ChannelID, rcmdsStr)
				
				//session.ChannelMessageSend(message.ChannelID, fmt.Sprintf("Poll count: %v\n", len(polls)))
			case "restart": // Restart EthanBot
				session.ChannelMessageSend(message.ChannelID, "Restarting...")
				shutdn<-true
			case "reset":
				os.Exit(1)
			default:
				session.ChannelMessageSend(message.ChannelID, arg+" is not a sub-command!")
			}
		},
	},
	"host": &command {
		main: func(arg string, session *discordgo.Session, message *discordgo.MessageCreate) {
			switch arg {
			case "poweroff": // Shut down host
				session.ChannelMessageSend(message.ChannelID, "Shutting down host...")
				exec.Command("systemctl", "poweroff").Start()
				shutdn<-false
			case "reboot": // Reboot host
				session.ChannelMessageSend(message.ChannelID, "Rebooting host...")
				exec.Command("systemctl", "reboot").Start()
				shutdn<-false
			default:
				session.ChannelMessageSend(message.ChannelID, arg+" is not a sub-command!")
			}
		},
	},
	"calnow": &command {
		main: func(arg string, session *discordgo.Session, message *discordgo.MessageCreate) {
			calnow <- calnow_normal
		},
	},
	"calnowquiet": &command {
		main: func(arg string, session *discordgo.Session, message *discordgo.MessageCreate) {
			calnow <- calnow_quiet
		},
	},
	"announce": &command {
		main: func(arg string, session *discordgo.Session, message *discordgo.MessageCreate) {
			session.ChannelMessageSend(announceChannel, arg)
		},
	},
	"del": &command{
		main: func(arg string, session *discordgo.Session, message *discordgo.MessageCreate) {
			err := session.ChannelMessageDelete(message.ChannelID, arg)
			if err != nil { reportErr("$ del failed", err) }
		//	// Delete this message ("$ del")
		//	err = session.ChannelMessageDelete(message.ChannelID, message.ID)
		//	if err != nil { reportErr("$ del failed", err) }
		},
	},
	// TODO: have an $ everyone command! It messages: "@everyone\n"+arg, and deletes the original message. Similar to $ del
}
