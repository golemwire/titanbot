// TODO: NOTE: you'll need to use syncing

package main

import (
	"github.com/bwmarrin/discordgo"
)

type Poll struct{
	ownerID string   // TODO: make these a [18]byte (or whatever) buffer for speed!
	messageID string //
	channelID string //
	
	yesses uint32
	nos uint32
}

var (
	polls = make([]*Poll, 8)
	pollsByMID = map[string]*Poll{}
	
	pollingUsers = map[string]bool{}
)

func (p *Poll)conclude() {
	// TODO: remove the poll from the polls slice
	
	user, _ := bot.User(p.ownerID)
	edit := user.Mention()+"'s poll\nPoll finished! It's "+func() string {
		if p.yesses < p.nos {
			return "👎!"
		} else if p.yesses > p.nos {
			return "👍!"
		} else {
			return "a tie!"
		}
	}()
	
	// TODO: edit the poll message to the edit string
	//discordgo.NewMessageEdit(p.channelID, p.messageID).SetContent(edit)
	bot.ChannelMessageEdit(p.channelID, p.messageID, edit)
	
	// TODO: remove EthanBot's reactions
}

func concludePollByUID(uid string) {
	// TODO: finish!
}

func addPoll(poll *Poll) {
	polls = append(polls, poll)
}

func onReact(session *discordgo.Session, message *discordgo.MessageReactionAdd) {
	// TODO: code!
}
func onReactRemove(session *discordgo.Session, message *discordgo.MessageReactionRemove) {
	// TODO: code!
}
func onReactRemoveAll(session *discordgo.Session, message *discordgo.MessageReactionRemoveAll) {
	// TODO: code!
	// Perhaps kick out an error and cancel the poll.
}
